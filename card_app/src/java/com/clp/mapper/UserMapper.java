package com.clp.mapper;

import com.clp.model.User;

/**
 * 采用了Mybatis的注解方式去实现DAO成访问
 * 接口需要在**Mapper.xml中配置
 * 接口中的方法对应**Mapper.xml的select.id
 * @ClassName: UserMapper 
 * @Description: TODO
 * @author: Administrator
 * @date: 2017年6月10日 下午5:22:15
 */
public interface UserMapper {
    public User findUserInfo();
}
